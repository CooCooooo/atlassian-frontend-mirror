import Page from '@atlaskit/webdriver-runner/wd-app-wrapper';
import { ADFEntity } from '@atlaskit/adf-utils';

import { testMediaFileId, defaultCollectionName } from '../_mocks/utils';

export const SELECTORS_WEB = {
  EDITOR: '#editor .ProseMirror',
  RENDERER: '#renderer',
};

/**
 * Fetch the ADF document as a JSON object from the editor
 */
export async function getADFContent(page: Page): Promise<ADFEntity> {
  await page.switchToWeb();
  return page.execute<ADFEntity>(() =>
    JSON.parse((window as any).bridge.getContent()),
  );
}

/**
 * Load an ADF document into the editor or renderer.
 *
 * @param adf The JSON representation of the ADF document.
 */
export async function setADFContent(
  page: Page,
  adf: ADFEntity,
  bridge: 'editor' | 'renderer' = 'editor',
) {
  await page.switchToWeb();
  const bridgeKey = bridge === 'renderer' ? 'rendererBridge' : 'bridge';
  return page.execute(
    (_adf, _bridgeKey) => {
      (window as any)[_bridgeKey].setContent(_adf);
    },
    adf,
    bridgeKey,
  );
}

/**
 * Clear all content from the editor back to a blank slate.
 */
export async function clearContent(page: Page) {
  await page.switchToWeb();
  return page.execute<void>(() => (window as any).bridge.clearContent());
}

/**
 * Mimic the flow of bridge calls to upload a media item
 */
export async function uploadMedia(page: Page) {
  const defaultFileAttrs = {
    id: 'null',
    name: 'test',
    type: 'image/png',
    dimensions: {
      width: 220,
      height: 140,
    },
  };
  await page.switchToWeb();
  await page.execute<void>(defaultFileAttrs => {
    (window as any).bridge.onMediaPicked(
      'upload-preview-update',
      JSON.stringify({
        file: defaultFileAttrs,
      }),
    );
  }, defaultFileAttrs);

  const uploadEndFile = {
    ...defaultFileAttrs,
    publicId: testMediaFileId,
    collectionName: defaultCollectionName,
  };
  await page.execute<void>(file => {
    (window as any).bridge.onMediaPicked(
      'upload-end',
      JSON.stringify({
        file,
      }),
    );
  }, uploadEndFile);
}
