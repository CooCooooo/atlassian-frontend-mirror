import { CSSObject } from '@emotion/core';

import { N800 } from '@atlaskit/theme/colors';

export const headingStyle: CSSObject = {
  display: 'flex',
  padding: '0 0 8px 0',
  fontWeight: 'bold',
  color: N800,
};

export const monthAndYearStyle: CSSObject = {
  flexBasis: '100%',
  textAlign: 'center',
};

export const arrowLeftStyle: CSSObject = {
  marginLeft: 8,
};

export const arrowRightStyle: CSSObject = {
  marginRight: 8,
};
