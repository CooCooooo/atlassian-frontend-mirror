export { default } from './components/Icon';
export { sizeMap as size, sizes } from './constants';
export { default as Skeleton } from './components/Skeleton';

export type { Size } from './types';
export type { SkeletonProps } from './components/Skeleton';
export type { IconProps } from './components/Icon';
