/**
 * Delete this entrypoint when moving to use the base icon from `@atlaskit/icon`.
 */
export { default } from '../components/Icon';
