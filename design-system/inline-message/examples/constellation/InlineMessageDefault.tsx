import React from 'react';

import InlineMessage from '../../src';

export default () => (
  <InlineMessage title="Title" secondaryText="Secondary text">
    <p>Default type dialog</p>
  </InlineMessage>
);
