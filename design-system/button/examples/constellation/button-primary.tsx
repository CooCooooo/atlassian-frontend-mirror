import React from 'react';

import Button from '../../src';

export default () => <Button appearance="primary">Primary button</Button>;
