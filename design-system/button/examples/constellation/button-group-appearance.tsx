import React from 'react';

import Button, { ButtonGroup } from '../../src';

export default () => (
  <ButtonGroup appearance="primary">
    <Button>First button</Button>
    <Button>Second button</Button>
    <Button>Third button</Button>
  </ButtonGroup>
);
