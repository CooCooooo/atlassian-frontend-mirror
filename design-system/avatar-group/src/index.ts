export { default } from './components/AvatarGroup';
export type { AvatarGroupProps } from './components/AvatarGroup';
export type { AvatarProps } from './components/types';
