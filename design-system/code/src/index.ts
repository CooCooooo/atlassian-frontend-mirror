export { default as Code } from './ThemedCode';
export { default as CodeBlock } from './ThemedCodeBlock';

export type { SupportedLanguages } from './supportedLanguages';
export type { CodeBlockTheme, CodeTheme } from './themes/types';
