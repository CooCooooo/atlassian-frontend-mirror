import React from 'react';

import {
  AtlassianIcon,
  AtlassianLogo,
  AtlassianWordmark,
  BitbucketIcon,
  BitbucketLogo,
  BitbucketWordmark,
  ConfluenceIcon,
  ConfluenceLogo,
  ConfluenceWordmark,
  HipchatIcon,
  HipchatLogo,
  HipchatWordmark,
  JiraCoreIcon,
  JiraCoreLogo,
  JiraCoreWordmark,
  JiraIcon,
  JiraLogo,
  JiraServiceDeskIcon,
  JiraServiceDeskLogo,
  JiraServiceDeskWordmark,
  JiraServiceManagementIcon,
  JiraServiceManagementLogo,
  JiraServiceManagementWordmark,
  JiraSoftwareIcon,
  JiraSoftwareLogo,
  JiraSoftwareWordmark,
  JiraWordmark,
  OpsGenieIcon,
  OpsGenieLogo,
  OpsGenieWordmark,
  StatuspageIcon,
  StatuspageLogo,
  StatuspageWordmark,
  StrideIcon,
  StrideLogo,
  StrideWordmark,
  TrelloIcon,
  TrelloLogo,
  TrelloWordmark,
} from '../src';

export default () => (
  <div>
    <table>
      <thead>
        <tr>
          <th>Logo</th>
          <th>Wordmark</th>
          <th>Icon</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <AtlassianLogo />
          </td>
          <td>
            <AtlassianWordmark />
          </td>
          <td>
            <AtlassianIcon />
          </td>
        </tr>
        <tr>
          <td>
            <BitbucketLogo />
          </td>
          <td>
            <BitbucketWordmark />
          </td>
          <td>
            <BitbucketIcon />
          </td>
        </tr>
        <tr>
          <td>
            <ConfluenceLogo />
          </td>
          <td>
            <ConfluenceWordmark />
          </td>
          <td>
            <ConfluenceIcon />
          </td>
        </tr>
        <tr>
          <td>
            <HipchatLogo />
          </td>
          <td>
            <HipchatWordmark />
          </td>
          <td>
            <HipchatIcon />
          </td>
        </tr>
        <tr>
          <td>
            <JiraLogo />
          </td>
          <td>
            <JiraWordmark />
          </td>
          <td>
            <JiraIcon />
          </td>
        </tr>
        <tr>
          <td>
            <JiraCoreLogo />
          </td>
          <td>
            <JiraCoreWordmark />
          </td>
          <td>
            <JiraCoreIcon />
          </td>
        </tr>
        <tr>
          <td>
            <JiraServiceDeskLogo />
          </td>
          <td>
            <JiraServiceDeskWordmark />
          </td>
          <td>
            <JiraServiceDeskIcon />
          </td>
        </tr>
        <tr>
          <td>
            <JiraServiceManagementLogo />
          </td>
          <td>
            <JiraServiceManagementWordmark />
          </td>
          <td>
            <JiraServiceManagementIcon />
          </td>
        </tr>
        <tr>
          <td>
            <JiraSoftwareLogo />
          </td>
          <td>
            <JiraSoftwareWordmark />
          </td>
          <td>
            <JiraSoftwareIcon />
          </td>
        </tr>
        <tr>
          <td>
            <OpsGenieLogo />
          </td>
          <td>
            <OpsGenieWordmark />
          </td>
          <td>
            <OpsGenieIcon />
          </td>
        </tr>
        <tr>
          <td>
            <StatuspageLogo />
          </td>
          <td>
            <StatuspageWordmark />
          </td>
          <td>
            <StatuspageIcon />
          </td>
        </tr>
        <tr>
          <td>
            <StrideLogo />
          </td>
          <td>
            <StrideWordmark />
          </td>
          <td>
            <StrideIcon />
          </td>
        </tr>
        <tr>
          <td>
            <TrelloLogo />
          </td>
          <td>
            <TrelloWordmark />
          </td>
          <td>
            <TrelloIcon />
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);
