import { CardState } from '../../state/types';
import { InvokeHandler } from '../../model/invoke-handler';
import { CardAuthFlowOpts } from '../../state/context/types';
import { AnalyticsHandler } from '../../utils/types';

export type BlockCardProps = {
  url: string;
  cardState: CardState;
  authFlow?: CardAuthFlowOpts['authFlow'];
  handleAuthorize: (() => void) | undefined;
  handleErrorRetry: () => void;
  handlePreviewAnalytics: AnalyticsHandler;
  handleInvoke: InvokeHandler;
  handleFrameClick: React.EventHandler<React.MouseEvent | React.KeyboardEvent>;
  isSelected?: boolean;
  onResolve?: (data: { url?: string; title?: string }) => void;
  testId?: string;
  showActions?: boolean;
};
