# @atlaskit/charts

## 0.0.4

### Patch Changes

- [`043b95951cd`](https://bitbucket.org/atlassian/atlassian-frontend/commits/043b95951cd) - [ux] Adding Pie and Bar charts to the charts Package

## 0.0.3

### Patch Changes

- [`007103b93e6`](https://bitbucket.org/atlassian/atlassian-frontend/commits/007103b93e6) - [ux] ED-11993 Change behaviour of context panel so it will not push content if there is enough space to slide out without overlapping.
  Config panel will keep existing behaviour to push content if there isn't enough space to show without overlapping content. Also add width css transition to context panel content to mimic "slide in" animation.

  Add new shared const of `akEditorFullWidthLayoutLineLength` which indicates the line length of a full-width editor

- Updated dependencies

## 0.0.2

### Patch Changes

- [`a1b64d65f86`](https://bitbucket.org/atlassian/atlassian-frontend/commits/a1b64d65f86) - Hookup a configuration panel to the example

## 0.0.1

### Patch Changes

- [`1548e47a94d`](https://bitbucket.org/atlassian/atlassian-frontend/commits/1548e47a94d) - Add chart package
